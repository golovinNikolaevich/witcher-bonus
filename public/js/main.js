// Parallax start
const boxer = witcher.querySelector(".witcher__img"),
maxMove = witcher.offsetWidth / 30,
boxerCenterX = boxer.offsetLeft + (boxer.offsetWidth / 2),
boxerCenterY = boxer.offsetTop + (boxer.offsetHeight / 2),
fluidboxer = window.matchMedia("(min-width: 1226px)");

function getMousePos(xRef, yRef) {
  
  let panelRect = witcher.getBoundingClientRect();
  return {
	  x: Math.floor(xRef - panelRect.left) /(panelRect.right-panelRect.left)*witcher.offsetWidth,
	  y: Math.floor(yRef - panelRect.top) / (panelRect.bottom -panelRect.top) * witcher.offsetHeight
    };
}

document.body.addEventListener("mousemove", function(e) {
  let mousePos = getMousePos(e.clientX, e.clientY),
  distX = mousePos.x - boxerCenterX,
  distY = mousePos.y - boxerCenterY;
  if (Math.abs(distX) < 1900 && distY < 1200 && fluidboxer.matches) {
  boxer.style.transform = "translate("+(-1*distX)/29+"px,"+(-1*distY)/29+"px)";
    witcher.style.backgroundPosition = `calc(50% + ${distX/50}px) calc(50% + ${distY/50}px)`;
  }
})
// Parallax end

// Clipboard start
let clipboard = new ClipboardJS('.clipboard');

clipboard.on('success', function(e) {
  console.info('Action:', e.action);
  console.info('Text:', e.text);
  console.info('Trigger:', e.trigger);
});
  
function toClipboard(el) {
  el.setAttribute('data-clipboard-text',location.href+'');
  let clipboard = new ClipboardJS(el);
  
  clipboard.on('success', function(e) {
    console.info('Action:', e.action);
    console.info('Text:', e.text);
    console.info('Trigger:', e.trigger);
  });  
}
// Clipboard end